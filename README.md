# Mediathekview-Unraid
## About
This is a Unraid Docker template to run Mediathekview containerised.

The used image can be found [here](https://hub.docker.com/r/conrad784/mediathekview-webinterface) and the GitHub repository [here](https://github.com/conrad784/docker-mediathekview-webinterface).

<br>

## Download & Install

1. Download the .xml file with the following command:
    ```
    wget -O /boot/config/plugins/dockerMan/templates-user/my-Mediathekview.xml https://codeberg.org/Unraid-Docker-Templates/Mediathekview-Unraid/raw/branch/master/Mediathekview.xml
    ```

2. Go to the "Docker" tab in Unraid and scroll down to the bottom.
3. Click "ADD CONATINER".
4. Select the template "Mediathekview".
5. Configure the template.
6. Click "APPLY".
7. Wait a moment, then open the Web UI
8. Set the download location to "/output" in Mediathekview to get the downloads to your set location / media library.
9. Start downloading!